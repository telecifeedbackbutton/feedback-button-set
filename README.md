# README #

This README will include description of our Feddback button (further called FBB) which will be run as Javascript during learning session on edX.

### What is this repository for? ###

* Version 0.1
* Probably and firstly, implemented here: [tsc.edx.lv](https://tsc.edx.lv/)

### How it should work? ###

* FBB should consist of three options which are going to be displayed as GREEN (understand), YELLOW (neutral), and RED (not sure) colored boxes. Each box should be click-able and the mentioned colors will represent the overall understanding of the learning content in front of the learner. 
* FBB would function similar to pop-ups that are usualy present in many websites in order to provide help or feedback. It should be fixed on the screen so that the scrolling would not hide it, in other words it should be in learner's viewport at all times. It could disapear when learner has submitted his choice but re-appear again when laerner has moved on to another part of the course content.
* The initial idea is to use Javascript which could be embeded in edX. It should gather following data:
	+ Overall understanding
	+ Time
	* User
	* IP
	* Device
	* Browser
	+ Course
	* Content
	* Content type
	* Others that may be related

